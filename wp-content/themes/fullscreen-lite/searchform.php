<form method="get" id="searchform" role="search" action="<?php echo esc_url(home_url('/')); ?>" style="height:82px; display:flex; align-items:center">
	<div class="searchleft">
		<input type="text" value="" placeholder="<?php _e('Search','fullscreen-lite');?>" name="s" id="searchbox" class="searchinput" style="height:100%; font-family:Carrois Gothic"/>
	</div>
    <div class="searchright">
    	<input type="submit" class="submitbutton" value="<?php _e('Search','fullscreen-lite');?>" style="height:100%"/>
    </div>
    <div class="clearfix"></div>
</form>

