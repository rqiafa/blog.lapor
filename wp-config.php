<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'bloglapor');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '1?=!>lv;vowCVKsp~fQS[hq$,f2X[PuR_`VRyh)gc2rI}>6@Aie@upA6 M^es4`+');
define('SECURE_AUTH_KEY',  'H~B~g$;^nkOtE1gsE@GRSUA.fLJ`!5gu<Qx./]1;*)KXQaap|U_EPZALVY{[,_F-');
define('LOGGED_IN_KEY',    'PF{2*7|0N3QLjGU4GJO(J~fDuG6S1e^P_ypNJ>p:y`@azxx}Z]a@%d,u Yx8lZ8S');
define('NONCE_KEY',        'X`AEl1;f`xxa(1_m661Qx/U{1_KO+/,|pI6L.M=EbUH/GyjIBMi`G;#6Du0j} 5C');
define('AUTH_SALT',        'BiBxv@U{I5J~^8ZM}Sg#H@+=LnP2-hKOf6P+40)]T8]5>zPCjf*nSALDJ`$xg@d=');
define('SECURE_AUTH_SALT', 'ZSLBeVVSq8onT!}^^[IVI=kHp=i1BCw~#:?,N5|;ZM;6Azp9a[STpcrx#%Z-8},c');
define('LOGGED_IN_SALT',   'b2V+ih))abaf|z~EQV}BxXa`dE~EW>-YHt/r*9]|0N^H6-bmC{9;zy*Y|DPrigNA');
define('NONCE_SALT',       '?v+9D1(@beCpIRa;Y9%4:|_,XRNRGsdX:}Y,8{2,b[.rfJ{8+/}.xf+D;15L`-!g');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
